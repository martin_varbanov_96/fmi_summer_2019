\babel@toc {bulgarian}{}
\contentsline {section}{\numberline {1}\IeC {\CYRU }\IeC {\cyrv }\IeC {\cyro }\IeC {\cyrd }}{3}{section.1}% 
\contentsline {paragraph}{}{3}{section*.2}% 
\contentsline {paragraph}{}{3}{section*.3}% 
\contentsline {paragraph}{}{3}{section*.4}% 
\contentsline {paragraph}{}{3}{section*.5}% 
\contentsline {section}{\numberline {2}\IeC {\CYRD }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyrn }\IeC {\cyri }}{3}{section.2}% 
\contentsline {paragraph}{}{3}{section*.6}% 
\contentsline {paragraph}{}{4}{section*.7}% 
\contentsline {paragraph}{}{4}{section*.8}% 
\contentsline {paragraph}{}{5}{section*.9}% 
\contentsline {subsection}{\numberline {2.1}\IeC {\CYRK }\IeC {\cyro }\IeC {\cyrr }\IeC {\cyre }\IeC {\cyrl }\IeC {\cyra }\IeC {\cyrc }\IeC {\cyri }\IeC {\cyrya }}{5}{subsection.2.1}% 
\contentsline {paragraph}{}{5}{section*.10}% 
\contentsline {paragraph}{}{6}{section*.11}% 
\contentsline {paragraph}{}{6}{section*.12}% 
\contentsline {paragraph}{}{7}{section*.13}% 
\contentsline {subsection}{\numberline {2.2}\IeC {\CYRM }\IeC {\cyrn }\IeC {\cyro }\IeC {\cyrg }\IeC {\cyro }\IeC {\cyrr }\IeC {\cyre }\IeC {\cyrd }\IeC {\cyro }\IeC {\cyrv }\IeC {\cyri } \IeC {\cyrd }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyrn }\IeC {\cyri }}{7}{subsection.2.2}% 
\contentsline {paragraph}{}{8}{section*.14}% 
\contentsline {paragraph}{}{8}{section*.15}% 
\contentsline {paragraph}{}{8}{section*.16}% 
\contentsline {section}{\numberline {3}\IeC {\CYRM }\IeC {\cyro }\IeC {\cyrd }\IeC {\cyre }\IeC {\cyrl }\IeC {\cyri } \IeC {\cyri } \IeC {\CYRM }\IeC {\cyre }\IeC {\cyrt }\IeC {\cyro }\IeC {\cyrd }\IeC {\cyri }}{9}{section.3}% 
\contentsline {subsection}{\numberline {3.1}\IeC {\CYRCH }\IeC {\cyri }\IeC {\cyrs }\IeC {\cyrl }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyro } \IeC {\cyrd }\IeC {\cyri }\IeC {\cyrf }\IeC {\cyre }\IeC {\cyrr }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyrc }\IeC {\cyri }\IeC {\cyrr }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyre }}{9}{subsection.3.1}% 
\contentsline {paragraph}{}{9}{section*.17}% 
\contentsline {paragraph}{}{10}{section*.18}% 
\contentsline {paragraph}{}{10}{section*.19}% 
\contentsline {subsection}{\numberline {3.2}\IeC {\CYRM }\IeC {\cyre }\IeC {\cyrt }\IeC {\cyro }\IeC {\cyrd } \IeC {\cyrn }\IeC {\cyra } \IeC {\cyrn }\IeC {\cyra }\IeC {\cyrishrt }- \IeC {\cyrb }\IeC {\cyrhrdsn }\IeC {\cyrr }\IeC {\cyrz }\IeC {\cyro }\IeC {\cyrt }\IeC {\cyro } \IeC {\cyrs }\IeC {\cyrp }\IeC {\cyru }\IeC {\cyrs }\IeC {\cyrk }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyre }}{10}{subsection.3.2}% 
\contentsline {paragraph}{}{10}{section*.20}% 
\contentsline {paragraph}{}{10}{section*.21}% 
\contentsline {paragraph}{}{10}{section*.22}% 
\contentsline {paragraph}{}{10}{section*.23}% 
\contentsline {paragraph}{}{10}{section*.24}% 
\contentsline {subsection}{\numberline {3.3}\IeC {\CYRP }\IeC {\cyre }\IeC {\cyrr }\IeC {\cyrs }\IeC {\cyre }\IeC {\cyrp }\IeC {\cyrt }\IeC {\cyrr }\IeC {\cyro }\IeC {\cyrn }}{11}{subsection.3.3}% 
\contentsline {subsubsection}{\numberline {3.3.1}\IeC {\CYRM }\IeC {\cyre }\IeC {\cyrt }\IeC {\cyro }\IeC {\cyrd } \IeC {\cyrn }\IeC {\cyra } \IeC {\cyrn }\IeC {\cyra }\IeC {\cyrishrt }- \IeC {\cyrm }\IeC {\cyra }\IeC {\cyrl }\IeC {\cyrk }\IeC {\cyri }\IeC {\cyrt }\IeC {\cyre } \IeC {\cyrk }\IeC {\cyrv }\IeC {\cyra }\IeC {\cyrd }\IeC {\cyrr }\IeC {\cyra }\IeC {\cyrt }\IeC {\cyri }}{11}{subsubsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.4}\IeC {\CYRN }\IeC {\cyre }\IeC {\cyrv }\IeC {\cyrr }\IeC {\cyro }\IeC {\cyrn }\IeC {\cyrn }\IeC {\cyri } \IeC {\cyrm }\IeC {\cyrr }\IeC {\cyre }\IeC {\cyrzh }\IeC {\cyri }}{11}{subsection.3.4}% 
\contentsline {paragraph}{\IeC {\CYRP }\IeC {\cyrhrdsn }\IeC {\cyrr }\IeC {\cyrv }\IeC {\cyro }\IeC {\cyrn }\IeC {\cyra }\IeC {\cyrch }\IeC {\cyra }\IeC {\cyrl }\IeC {\cyre }\IeC {\cyrn } \IeC {\cyrm }\IeC {\cyro }\IeC {\cyrd }\IeC {\cyre }\IeC {\cyrl } \IeC {\cyrv } \IeC {\cyrt }\IeC {\cyre }\IeC {\cyrk }\IeC {\cyru }\IeC {\cyrshch } \IeC {\cyrm }\IeC {\cyro }\IeC {\cyrm }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyrt }}{11}{section*.25}% 
\contentsline {paragraph}{}{11}{section*.26}% 
\contentsline {subsection}{\numberline {3.5}\IeC {\CYRG }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyre }\IeC {\cyrt }\IeC {\cyri }\IeC {\cyrch }\IeC {\cyre }\IeC {\cyrn } \IeC {\CYRA }\IeC {\cyrl }\IeC {\cyrg }\IeC {\cyro }\IeC {\cyrr }\IeC {\cyri }\IeC {\cyrt }\IeC {\cyrhrdsn }\IeC {\cyrm }}{11}{subsection.3.5}% 
\contentsline {section}{\numberline {4}\IeC {\CYRZ }\IeC {\cyra }\IeC {\cyrk }\IeC {\cyrl }\IeC {\cyryu }\IeC {\cyrch }\IeC {\cyre }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyre }}{12}{section.4}% 
\contentsline {paragraph}{}{12}{section*.27}% 
\contentsline {paragraph}{}{12}{section*.28}% 
\contentsline {paragraph}{}{12}{section*.29}% 
\contentsline {section}{\numberline {5}Appendix}{12}{section.5}% 
\contentsline {subsection}{\numberline {5.1}\IeC {\CYRP }\IeC {\cyrhrdsn }\IeC {\cyrr }\IeC {\cyrv }\IeC {\cyro }\IeC {\cyrn }\IeC {\cyra }\IeC {\cyrch }\IeC {\cyra }\IeC {\cyrl }\IeC {\cyrn }\IeC {\cyri } \IeC {\cyri }\IeC {\cyrz }\IeC {\cyrs }\IeC {\cyrl }\IeC {\cyre }\IeC {\cyrd }\IeC {\cyrv }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyrya } \IeC {\cyrn }\IeC {\cyra } \IeC {\cyrd }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyrt }\IeC {\cyre }}{12}{subsection.5.1}% 
\contentsline {paragraph}{we start with 24000 rows}{12}{section*.30}% 
\contentsline {paragraph}{We drop about 70\% of data if we don't remove other cols or fill}{13}{section*.31}% 
\contentsline {paragraph}{We should remove other cols}{13}{section*.32}% 
\contentsline {paragraph}{Removing percipitation data:}{14}{section*.33}% 
\contentsline {paragraph}{We are up to only about 40\% data loss from missing data:}{14}{section*.34}% 
\contentsline {paragraph}{OK, we are removing WW}{16}{section*.35}% 
\contentsline {paragraph}{Removing the WW col}{19}{section*.36}% 
\contentsline {paragraph}{Lets check the correlation}{19}{section*.37}% 
\contentsline {paragraph}{First we have to numerize the string cols}{19}{section*.38}% 
\contentsline {paragraph}{array with classifier cols:}{20}{section*.39}% 
\contentsline {paragraph}{Remove the stringified cols}{20}{section*.40}% 
\contentsline {paragraph}{join the hotencoded cols}{20}{section*.41}% 
\contentsline {paragraph}{we got 80 cols}{20}{section*.42}% 
\contentsline {paragraph}{1 col is still object wise}{22}{section*.43}% 
\contentsline {paragraph}{removing time col}{22}{section*.44}% 
\contentsline {paragraph}{calc corr matrix}{23}{section*.45}% 
\contentsline {paragraph}{TODO: check which features are correlated}{23}{section*.46}% 
\contentsline {paragraph}{Temperature analysis}{26}{section*.47}% 
\contentsline {paragraph}{We got a couple of cols with corr above 20, 1 above 70}{28}{section*.48}% 
\contentsline {subsection}{\numberline {5.2}\IeC {\CYRM }\IeC {\cyrn }\IeC {\cyro }\IeC {\cyrg }\IeC {\cyro }\IeC {\cyrm }\IeC {\cyre }\IeC {\cyrr }\IeC {\cyrn }\IeC {\cyri } \IeC {\cyri }\IeC {\cyrz }\IeC {\cyrs }\IeC {\cyrl }\IeC {\cyre }\IeC {\cyrd }\IeC {\cyrv }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyrya } \IeC {\cyrn }\IeC {\cyra } \IeC {\cyrd }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyrn }\IeC {\cyri }\IeC {\cyrt }\IeC {\cyre }}{30}{subsection.5.2}% 
\contentsline {subsubsection}{\numberline {5.2.1}Set label future speed}{31}{subsubsection.5.2.1}% 
\contentsline {paragraph}{from 116 to 73 cols by removing empty}{32}{section*.49}% 
\contentsline {paragraph}{array with classifier cols:}{32}{section*.50}% 
\contentsline {paragraph}{Remove the stringified cols}{33}{section*.51}% 
\contentsline {paragraph}{join the hotencoded cols}{33}{section*.52}% 
\contentsline {paragraph}{we got 3441 cols}{33}{section*.53}% 
\contentsline {paragraph}{Corelated cols(more than 10\%)}{36}{section*.54}% 
\contentsline {paragraph}{Remove null cols}{38}{section*.55}% 
\contentsline {paragraph}{Cutting unneccessary cols}{38}{section*.56}% 
\contentsline {paragraph}{Final df}{38}{section*.57}% 
\contentsline {paragraph}{Export}{40}{section*.58}% 
\contentsline {subsection}{\numberline {5.3}Perceptron}{41}{subsection.5.3}% 
\contentsline {paragraph}{dot product}{41}{section*.59}% 
\contentsline {paragraph}{Definition}{41}{section*.60}% 
\contentsline {paragraph}{TODO: bias b in {[}0,1{]} ?}{41}{section*.61}% 
\contentsline {paragraph}{this is totaly failing:}{41}{section*.62}% 
\contentsline {paragraph}{I had better result in range {[}-1,-1{]}}{41}{section*.63}% 
\contentsline {paragraph}{trie in range {[}-10, 10{]} -\textgreater {} very moving bad result}{42}{section*.64}% 
\contentsline {paragraph}{the lower the bias, the better the result}{42}{section*.65}% 
\contentsline {paragraph}{Sample}{43}{section*.66}% 
\contentsline {paragraph}{test function}{44}{section*.67}% 
\contentsline {paragraph}{Basic differentiation}{44}{section*.68}% 
\contentsline {paragraph}{Gradient Descent}{44}{section*.69}% 
\contentsline {paragraph}{fucntion:}{44}{section*.70}% 
\contentsline {paragraph}{We have to have a gradient descent for:}{61}{section*.71}% 
\contentsline {subsubsection}{\numberline {5.3.1}Getting the data}{61}{subsubsection.5.3.1}% 
\contentsline {paragraph}{Import Data:}{61}{section*.72}% 
\contentsline {subsubsection}{\numberline {5.3.2}Gradient descent for vectors}{62}{subsubsection.5.3.2}% 
\contentsline {subsubsection}{\numberline {5.3.3}initial weights}{63}{subsubsection.5.3.3}% 
\contentsline {section}{\numberline {6}\IeC {\CYRB }\IeC {\cyri }\IeC {\cyrb }\IeC {\cyrl }\IeC {\cyri }\IeC {\cyro }\IeC {\cyrg }\IeC {\cyrr }\IeC {\cyra }\IeC {\cyrf }\IeC {\cyri }\IeC {\cyrya }}{63}{section.6}% 
