\babel@toc {bulgarian}{}
\contentsline {section}{\numberline {1}\IeC {\CYRU }\IeC {\cyrv }\IeC {\cyro }\IeC {\cyrd }}{3}{section.1}% 
\contentsline {paragraph}{}{3}{section*.2}% 
\contentsline {paragraph}{}{3}{section*.3}% 
\contentsline {paragraph}{}{3}{section*.4}% 
\contentsline {paragraph}{}{3}{section*.5}% 
\contentsline {section}{\numberline {2}\IeC {\CYRD }\IeC {\cyra }\IeC {\cyrn }\IeC {\cyrn }\IeC {\cyri }}{3}{section.2}% 
\contentsline {paragraph}{}{3}{section*.6}% 
\contentsline {paragraph}{}{4}{section*.7}% 
\contentsline {section}{\numberline {3}\IeC {\CYRM }\IeC {\cyro }\IeC {\cyrd }\IeC {\cyre }\IeC {\cyrl }\IeC {\cyri } \IeC {\cyri } \IeC {\CYRM }\IeC {\cyre }\IeC {\cyrt }\IeC {\cyro }\IeC {\cyrd }\IeC {\cyri }}{4}{section.3}% 
\contentsline {subsection}{\numberline {3.1}\IeC {\CYRN }\IeC {\cyre }\IeC {\cyrv }\IeC {\cyrr }\IeC {\cyro }\IeC {\cyrn }\IeC {\cyrn }\IeC {\cyri } \IeC {\cyrm }\IeC {\cyrr }\IeC {\cyre }\IeC {\cyrzh }\IeC {\cyri }}{4}{subsection.3.1}% 
\contentsline {section}{\numberline {4}Appendix}{4}{section.4}% 
\contentsline {paragraph}{we start with 24000 rows}{5}{section*.8}% 
\contentsline {paragraph}{We drop about 70\% of data if we don't remove other cols or fill}{6}{section*.9}% 
\contentsline {paragraph}{We should remove other cols}{6}{section*.10}% 
\contentsline {paragraph}{Removing percipitation data:}{6}{section*.11}% 
\contentsline {paragraph}{We are up to only about 40\% data loss from missing data:}{6}{section*.12}% 
\contentsline {paragraph}{OK, we are removing WW}{8}{section*.13}% 
\contentsline {paragraph}{Removing the WW col}{11}{section*.14}% 
\contentsline {paragraph}{Lets check the correlation}{11}{section*.15}% 
\contentsline {paragraph}{First we have to numerize the string cols}{12}{section*.16}% 
\contentsline {paragraph}{array with classifier cols:}{13}{section*.17}% 
\contentsline {paragraph}{Remove the stringified cols}{13}{section*.18}% 
\contentsline {paragraph}{join the hotencoded cols}{13}{section*.19}% 
\contentsline {paragraph}{we got 80 cols}{13}{section*.20}% 
\contentsline {paragraph}{1 col is still object wise}{15}{section*.21}% 
\contentsline {paragraph}{removing time col}{15}{section*.22}% 
\contentsline {paragraph}{calc corr matrix}{15}{section*.23}% 
\contentsline {paragraph}{TODO: check which features are correlated}{15}{section*.24}% 
\contentsline {paragraph}{Temperature analysis}{18}{section*.25}% 
\contentsline {paragraph}{We got a couple of cols with corr above 20, 1 above 70}{21}{section*.26}% 
