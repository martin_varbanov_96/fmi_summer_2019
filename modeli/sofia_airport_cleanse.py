#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 18:48:30 2019

@author: martin
"""

import pandas as pd
path = "data/sofia_2.csv"
df = pd.read_csv(path)
df_nan = df.dropna()

# print nulls
df.isnull().sum()

null_cols = ["ff10", "ff3", "WW1", "WW2", "E", "Tg", "E'", "sss"]
